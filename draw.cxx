#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include <iostream>
#include <fstream>
#include "stdio.h"
#include <string>

void draw(){
    gStyle->SetOptStat(0);

    auto file = new TFile("ana.root");
    auto tree = (TTree*)file->Get("tree");

    int channel;
    float charge_ToT; 

    auto h_channel = new TH1F("Channel ID", "Channel ID", 66, -1, 65);
    auto h_charge = new TH1F("Charge ToT", "Charge ToT", 100, -1500, 1000);
    auto charge_channel = new TH2F("ChargeToT vs Channel", "ChargeToT vs Channel", 66, -1, 65, 100, -1500, 1000);

    tree->SetBranchAddress("channel",&channel);
    tree->SetBranchAddress("charge_ToT",&charge_ToT);


    for (int i = 0; i < tree->GetEntries(); i++) {
	tree->GetEntry(i);
	h_channel->Fill(channel);
	h_charge->Fill(charge_ToT);
	charge_channel->Fill(channel, charge_ToT);
    }


    auto c1 = new TCanvas("c1","c1",1200,800);
    c1->cd();
    charge_channel->GetXaxis()->SetTitle("channel");
    charge_channel->GetYaxis()->SetTitle("charge_ToT");
    charge_channel->Draw("colz");
    c1->Print("charge_channel.pdf");

    auto c2 = new TCanvas("c2","c2",1200,800);
    c2->cd();
    h_channel->GetXaxis()->SetTitle("channel");
    h_channel->GetYaxis()->SetTitle("entries");
    h_channel->Draw();
    c2->Print("channel.pdf");

    auto c3 = new TCanvas("c3","c3",1200,800);
    c3->cd();
    h_charge->GetXaxis()->SetTitle("charge ToT");
    h_charge->GetYaxis()->SetTitle("entries");
    h_charge->Draw();
    c3->Print("charge_ToT.pdf");

    file->Close();

}


